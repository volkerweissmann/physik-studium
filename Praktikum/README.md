Hier finden sich meine Protokolle der Praktikas. Sonderlich gut sind sie nicht
(mangels Motivation) aber sie wurden akzeptiert. Ich bin mir allerdings nicht
sicher, ob die Version die hier ist auch tatsächlich die akzeptierte Version
ist. Manchmal habe auch ich angefangen zu schreiben, dann es meinem Partner
geschickt, er hat es fertig gemacht und es abgeschickt. Dann fehlt im Protokoll
hier die Hälfte.

Wer Rohdaten oder LaTeX-Sourcecode braucht soll mich unter anschreiben
(volker.weissmann@gmx.de).
